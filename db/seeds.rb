(1..100).each do |i|
  Product.create label: Faker::Commerce.product_name, unit_price_et: 10.0, vat: 20.0
end

(1..100).each do |i|
  Customer.create first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: "#{Faker::Internet.username}@mail#{i}.com"
end
