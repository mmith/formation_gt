class AddStateToOrder < ActiveRecord::Migration[7.1]
  def change
    add_column :orders, :state, :string
    add_index :orders, :state
  end
end
