class RenameSkus < ActiveRecord::Migration[7.1]
  def change
    remove_index :orders, :order_sku
    rename_column :orders, :order_sku, :sku
    add_index :orders, :sku, unique: true

    rename_column :invoices, :invoice_sku, :sku
    add_index :invoices, :sku, unique: true
  end
end
