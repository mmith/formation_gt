class RemovePolymorphics < ActiveRecord::Migration[7.1]
  def change

    # remove_index :items, :type
    # remove_index :items, :order_id
    # remove_index :items, :product_id

    drop_table :items

    create_table :invoice_items do |t|
      t.integer :invoice_id
      t.integer :product_id
      t.float :unit_price_et
      t.float :vat
      t.integer :quantity

      t.timestamps
    end
    add_index :invoice_items, :invoice_id
    add_index :invoice_items, :product_id
  end
end
