class AddStateToProduct < ActiveRecord::Migration[7.1]
  def change
    add_column :products, :state, :string
  end
end
