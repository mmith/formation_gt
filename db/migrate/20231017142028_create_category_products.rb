class CreateCategoryProducts < ActiveRecord::Migration[7.1]
  def change
    create_table :category_products do |t|
      t.integer :category_id
      t.integer :product_id

      t.timestamps
    end
    add_index :category_products, :category_id
    add_index :category_products, :product_id
  end
end
