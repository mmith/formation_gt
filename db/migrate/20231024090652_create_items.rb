class CreateItems < ActiveRecord::Migration[7.1]
  def change
    create_table :items do |t|
      t.string :type
      t.integer :order_id
      t.integer :product_id
      t.float :unit_price_et
      t.float :vat
      t.integer :quantity

      t.timestamps
    end
    add_index :items, :type
    add_index :items, :order_id
    add_index :items, :product_id
  end
end
