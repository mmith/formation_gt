class CreateProducts < ActiveRecord::Migration[7.1]
  def change
    create_table :products do |t|
      t.string :label
      t.float :unit_price_et
      t.float :vat

      t.timestamps
    end
  end
end
