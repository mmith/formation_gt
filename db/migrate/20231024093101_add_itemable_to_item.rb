class AddItemableToItem < ActiveRecord::Migration[7.1]
  def change
    add_column :items, :itemable_id, :integer
    add_column :items, :itemable_type, :string

    add_index :items, [:itemable_id, :itemable_type], name: 'idx_itemable'

    remove_index :items, :order_id
    remove_column :items, :order_id
  end
end
