class AddAddressableToAddress < ActiveRecord::Migration[7.1]
  def change
    add_column :addresses, :addressable_id, :integer
    add_column :addresses, :addressable_type, :string

    add_index :addresses, [:addressable_id, :addressable_type], name: 'idx_addressable'
  end
end
