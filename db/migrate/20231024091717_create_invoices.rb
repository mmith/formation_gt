class CreateInvoices < ActiveRecord::Migration[7.1]
  def change
    create_table :invoices do |t|
      t.integer :order_id
      t.string :invoice_sku
      t.integer :customer_id

      t.timestamps
    end
    add_index :invoices, :order_id
    add_index :invoices, :customer_id
  end
end
