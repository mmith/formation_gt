class CreateOrders < ActiveRecord::Migration[7.1]
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.string :order_sku

      t.timestamps
    end
    add_index :orders, :customer_id
    add_index :orders, :order_sku, unique: true
  end
end
