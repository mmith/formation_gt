FactoryBot.define do
  factory :customer do
    last_name { 'test' }
    first_name { 'test' }
    email { 'test@test.com' }
  end
end
