FactoryBot.define do
  factory :order_item do
    product
    order
    quantity { 1 }
    unit_price_et { 10.0 }
    vat { 20.0 }
  end
end
