FactoryBot.define do
  factory :item do
    type { "" }
    order_id { 1 }
    product_id { 1 }
    unit_price_et { 1.5 }
    vat { 1.5 }
    quantity { 1 }
  end
end
