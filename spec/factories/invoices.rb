FactoryBot.define do
  factory :invoice do
    customer
    order
    sku { Time.now.to_i.to_s }
  end
end
