FactoryBot.define do
  factory :product do
    label { "TEst" }
    unit_price_et { 20.0 }
    vat { 20.0 }
  end
end
