require 'rails_helper'

RSpec.describe CustomersController, type: :controller do

  context 'JSON API for collections' do
    describe "GET index" do
      before(:each) do
        get :index, format: :json
      end

      it 'should assign customers' do
        expect(assigns(:customers).nil?).to eq false
      end

      it { should respond_with(:success) }

    end

    describe "GET new" do
      before(:each) do
        get :new, format: :json
      end

      it 'should assign new customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq true
        expect(assigns(:customer).is_a?(Customer)).to eq true
      end

      it { should respond_with(:success) }
    end

    describe "POST create with valid params" do
      before(:each) do
        @customer = build(:customer)
        post :create, params: { customer: @customer.attributes }, format: :json
      end

      it 'should assign customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq false
        expect(assigns(:customer).is_a?(Customer)).to eq true
      end

      it { should respond_with(:success) }
    end

    describe "POST create with invalid params" do
      before(:each) do
        @customer = build(:customer)
        @customer.first_name = ''
        post :create, params: { customer: @customer.attributes }, format: :json
      end

      it 'should assign customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq true
        expect(assigns(:customer).is_a?(Customer)).to eq true
      end

      it { should respond_with(406) }
    end
  end

  context 'JSON API for members' do
    before(:each) do
      @customer = create(:customer)
    end

    describe 'GET show' do
      before(:each) do
        get :show, params: { id: @customer.id }, format: :json
      end

      it 'should assign customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq false
      end

      it { should respond_with(:success) }
    end

    describe 'PATCH update with valid params' do
      before(:each) do
        @customer.first_name = 'Rspec label'
        patch :update, params: { id: @customer.id, customer: @customer.attributes }, format: :json
      end

      it 'should assign customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq false
      end

      it 'should update customer' do
        expect(assigns(:customer).first_name).to eq 'Rspec label'
      end

      it { should respond_with(:success) }
    end

    describe 'PATCH update with invalid params' do
      before(:each) do
        @customer.first_name = ''
        patch :update, params: { id: @customer.id, customer: @customer.attributes }, format: :json
      end

      it 'should assign customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq false
      end

      it { should respond_with(406) }
    end

    describe 'DELETE destroy with valid params' do
      before(:each) do
        delete :destroy, params: { id: @customer.id }, format: :json
      end

      it 'should assign customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq false
      end

      it { should respond_with(:success) }
    end

    describe 'DELETE destroy with invalid params' do
      before(:each) do
        @order = create(:order, customer_id: @customer.id)
        delete :destroy, params: { id: @customer.id }, format: :json
      end

      it 'should assign customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq false
      end

      it { should respond_with(406) }
    end
  end

  context 'HTML format for collections' do
    describe "GET index" do
      before(:each) do
        get :index
      end

      it 'should assign customers' do
        expect(assigns(:customers).nil?).to eq false
      end

      it { should respond_with(:success) }
      it { should render_template(:index) }
      it { should render_with_layout(:application) }

    end

    describe "GET new" do
      before(:each) do
        get :new
      end

      it 'should assign new customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq true
        expect(assigns(:customer).is_a?(Customer)).to eq true
      end

      it { should respond_with(:success) }
      it { should render_template(:new) }
      it { should render_with_layout(:application) }

    end

    describe "POST create with valid params" do
      before(:each) do
        @customer = build(:customer)
        post :create, params: { customer: @customer.attributes }
      end

      it 'should assign customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq false
        expect(assigns(:customer).is_a?(Customer)).to eq true
      end

      it { should respond_with(:redirect) }
      it { should redirect_to(customers_path) }

      it 'should set flash message' do
        expect(response.request.flash[:success]).to_not be_nil
        expect(response.request.flash[:success]).to eq I18n.t('flash.success_save')
      end
    end

    describe "POST create with invalid params" do
      before(:each) do
        @customer = build(:customer)
        @customer.first_name = ''
        post :create, params: { customer: @customer.attributes }
      end

      it 'should assign customer' do
        expect(assigns(:customer).nil?).to eq false
        expect(assigns(:customer).new_record?).to eq true
        expect(assigns(:customer).is_a?(Customer)).to eq true
      end

      it { should respond_with(:success) }
      it { should render_template(:new) }
      it { should render_with_layout(:application) }
    end
  end
end
