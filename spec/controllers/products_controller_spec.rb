require 'rails_helper'

RSpec.describe ProductsController, type: :controller do

  context 'JSON API for collections' do
    describe "GET index" do
      before(:each) do
        get :index, format: :json
      end

      it 'should assign products' do
        expect(assigns(:products).nil?).to eq false
      end

      it { should respond_with(:success) }

    end

    describe "GET enableds" do
      before(:each) do
        get :enableds, format: :json
      end

      it 'should assign products' do
        expect(assigns(:products).nil?).to eq false
      end

      it { should respond_with(:success) }

    end

    describe "GET disableds" do
      before(:each) do
        get :disableds, format: :json
      end

      it 'should assign products' do
        expect(assigns(:products).nil?).to eq false
      end

      it { should respond_with(:success) }

    end

    describe "GET new" do
      before(:each) do
        get :new, format: :json
      end

      it 'should assign new product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq true
        expect(assigns(:product).is_a?(Product)).to eq true
      end

      it { should respond_with(:success) }
    end

    describe "POST create with valid params" do
      before(:each) do
        @product = build(:product)
        post :create, params: { product: @product.attributes }, format: :json
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq false
        expect(assigns(:product).is_a?(Product)).to eq true
      end

      it { should respond_with(:success) }
    end

    describe "POST create with invalid params" do
      before(:each) do
        @product = build(:product)
        @product.label = ''
        post :create, params: { product: @product.attributes }, format: :json
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq true
        expect(assigns(:product).is_a?(Product)).to eq true
      end

      it { should respond_with(406) }
    end
  end

  context 'JSON API for members' do
    before(:each) do
      @product = create(:product)
    end

    describe 'GET show' do
      before(:each) do
        get :show, params: { id: @product.id }, format: :json
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq false
      end

      it { should respond_with(:success) }
    end

    describe 'PATCH update with valid params' do
      before(:each) do
        @product.label = 'Rspec label'
        patch :update, params: { id: @product.id, product: @product.attributes }, format: :json
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq false
      end

      it 'should update product' do
        expect(assigns(:product).label).to eq 'Rspec label'
      end

      it { should respond_with(:success) }
    end

    describe 'PATCH update with invalid params' do
      before(:each) do
        @product.label = ''
        patch :update, params: { id: @product.id, product: @product.attributes }, format: :json
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq false
      end

      it { should respond_with(406) }
    end

    describe 'DELETE destroy with valid params' do
      before(:each) do
        delete :destroy, params: { id: @product.id }, format: :json
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq false
      end

      it { should respond_with(:success) }
    end

    describe 'DELETE destroy with invalid params' do
      before(:each) do
        @order_item = create(:order_item, product_id: @product.id)
        delete :destroy, params: { id: @product.id }, format: :json
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq false
      end

      it { should respond_with(406) }
    end

    describe 'PATCH enable from enabled' do
      before(:each) do
        patch :enable, params: { id: @product.id }, format: :json
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq false
      end

      it { should respond_with(406)}
    end

    describe 'PATCH enable from disabled' do
      before(:each) do
        @product.disable
        patch :enable, params: { id: @product.id }, format: :json
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq false
      end

      it 'should change product state' do
        expect(assigns(:product).enabled?).to eq true
      end

      it { should respond_with(:success)}
    end
  end


  context 'HTML format for collections' do
    describe "GET index" do
      before(:each) do
        get :index
      end

      it 'should assign products' do
        expect(assigns(:products).nil?).to eq false
      end

      it { should respond_with(:success) }
      it { should render_template(:index) }
      it { should render_with_layout(:application) }

    end

    describe "GET new" do
      before(:each) do
        get :new
      end

      it 'should assign new product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq true
        expect(assigns(:product).is_a?(Product)).to eq true
      end

      it { should respond_with(:success) }
      it { should render_template(:new) }
      it { should render_with_layout(:application) }

    end

    describe "POST create with valid params" do
      before(:each) do
        @product = build(:product)
        post :create, params: { product: @product.attributes }
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq false
        expect(assigns(:product).is_a?(Product)).to eq true
      end

      it { should respond_with(:redirect) }
      it { should redirect_to(products_path) }

      it 'should set flash message' do
        expect(response.request.flash[:success]).to_not be_nil
        expect(response.request.flash[:success]).to eq I18n.t('flash.success_save')
      end
    end

    describe "POST create with invalid params" do
      before(:each) do
        @product = build(:product)
        @product.label = ''
        post :create, params: { product: @product.attributes }
      end

      it 'should assign product' do
        expect(assigns(:product).nil?).to eq false
        expect(assigns(:product).new_record?).to eq true
        expect(assigns(:product).is_a?(Product)).to eq true
      end

      it { should respond_with(:success) }
      it { should render_template(:new) }
      it { should render_with_layout(:application) }
    end
  end
end
