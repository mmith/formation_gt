require 'rails_helper'

RSpec.describe Order, type: :model do
  it { is_expected.to have_db_column(:id).of_type(:integer)}
  it { is_expected.to have_db_column(:customer_id).of_type(:integer)}
  it { is_expected.to have_db_column(:sku).of_type(:string)}
  it { is_expected.to have_db_column(:state).of_type(:string)}
  it { is_expected.to have_db_column(:created_at).of_type(:datetime)}
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime)}
  it { is_expected.to have_db_index(:customer_id)}
  it { is_expected.to have_db_index(:sku)}
  it { is_expected.to have_db_index(:state)}
end

RSpec.describe Order, type: :model do
  subject { Order.new }

  it { should belong_to(:customer) }
  it { should have_many(:order_items).dependent(:destroy) }

  it { should validate_presence_of(:customer_id) }
  it { should validate_numericality_of(:customer_id) }
end

RSpec.describe Order, type: :model do
  before do
    @order = create(:order)
  end

  it 'should be valid' do
    expect(@order.valid?).to be true
  end

  it 'should generate sku before create' do
    count = Order.count
    expect(@order.sku).to eq "O"+Date.today.year.to_s + Date.today.month.to_s.rjust(2,'0') + (count).to_s
  end
end

RSpec.describe Order, type: :model do
  before do
    @order = create(:order)
  end

  it 'should be pending at initial state' do
    expect(@order.pending?).to be true
  end

  it 'should be enabled after enable' do
    @order.enable
    expect(@order.enabled?).to be true
  end

  it 'should be cancelled after cancel' do
    @order.enable
    @order.cancel
    expect(@order.canceled?).to be true
  end

  it 'should be prepared after make' do
    @order.enable
    @order.make
    expect(@order.prepared?).to be true
  end

  it 'should be dispatched after dispatch' do
    @order.enable
    @order.make
    @order.dispatch
    expect(@order.dispatched?).to be true
  end

  it 'should be received after receive' do
    @order.enable
    @order.make
    @order.dispatch
    @order.receive
    expect(@order.received?).to be true
  end
end
