require 'rails_helper'

RSpec.describe Customer, type: :model do
  it { is_expected.to have_db_column(:first_name).of_type(:string)}
  it { is_expected.to have_db_column(:last_name).of_type(:string)}
  it { is_expected.to have_db_column(:email).of_type(:string)}
  it { is_expected.to have_db_column(:state).of_type(:string)}
  it { is_expected.to have_db_column(:created_at).of_type(:datetime)}
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime)}
end

RSpec.describe Customer, type: :model do
  subject { Customer.new }

  it { should have_many(:orders) }

  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:email) }
end

RSpec.describe Customer, type: :model do
  before do
    @customer = build(:customer)
  end

  it 'should be valid' do
    expect(@customer.valid?).to be true
  end

  it 'should not be valid with invalid email' do
    @customer.email = 'sdsqqsdq'
    expect(@customer.valid?).to be false
  end

  it 'should be valid with valid email' do
    @customer.email = 'sdsq@qsdq.com'
    expect(@customer.valid?).to be true
  end
end
