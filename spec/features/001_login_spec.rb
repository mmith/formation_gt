require 'rails_helper'

RSpec.feature "001 Logins", type: :feature do
  it 'should display login form' do
    visit root_path

    expect(page).to have_content('Espace privé')
    expect(page).to have_content('Connexion')
    expect(page).to have_field('Email')
    expect(page).to have_field('Password')
    expect(page).to have_button('Se connecter')

    click_button "Se connecter"

    expect(page).to have_content('Invalid Email or password.')



  end
end
