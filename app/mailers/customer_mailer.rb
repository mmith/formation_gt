class CustomerMailer < ApplicationMailer
  def send_informations(customer)
    @customer = customer
    attachments.inline['logo.png'] = File.read('./app/assets/images/logo.png')
    mail(to: @customer.email, subject: "Vos informations")
  end
end
