class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :product

  validates :order_id, presence: true
  validates :order_id, numericality: true
  validates :product_id, presence: true
  validates :product_id, numericality: true
  validates :unit_price_et, presence: true
  validates :vat, presence: true
  validates :unit_price_et, numericality: true
  validates :vat, numericality: true
end
