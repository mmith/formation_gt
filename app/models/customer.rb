class Customer < ApplicationRecord
  has_many :orders, dependent: :restrict_with_error
  has_many :addresses, as: :addressable, dependent: :destroy

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true
  validates :email, email: true, allow_blank: true

  def full_name
    first_name + ' ' + last_name
  end
end
