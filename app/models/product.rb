class Product < ApplicationRecord
  has_many :order_items, dependent: :restrict_with_error
  # has_many :order_items, dependent: :restrict_with_exception

  has_many :category_products, dependent: :destroy
  has_many :categories, through: :category_products

  validates :label, presence: true
  validates :unit_price_et, presence: true
  validates :vat, presence: true
  validates :unit_price_et, numericality: true
  validates :vat, numericality: true

  default_scope -> { order('label ASC') }
  scope :disableds, -> { where(state: 'disabled') }
  scope :enableds, -> { where(state: 'enabled') }

  state_machine :state, initial: :enabled do
    event :enable do
      transition disabled: :enabled
    end

    event :disable do
      transition enabled: :disabled
    end
  end
end
