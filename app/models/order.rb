class Order < ApplicationRecord
  include SkuConcern

  belongs_to :customer
  has_many :order_items, dependent: :destroy
  has_many :addresses, as: :addressable, dependent: :destroy

  validates :customer_id, presence: true
  validates :customer_id, numericality: true

  accepts_nested_attributes_for :order_items, reject_if: :all_blank, allow_destroy: true

  state_machine :state, initial: :pending do
    event :enable do
      transition pending: :enabled
    end

    event :cancel do
      transition [:enabled, :prepared] => :canceled
    end

    event :make do
      transition enabled: :prepared
    end

    event :dispatch do
      transition prepared: :dispatched
    end

    event :receive do
      transition dispatched: :received
    end
  end
end
