class InvoiceItem < ApplicationRecord
  belongs_to :invoice_item
  belongs_to :product

  validates :invoice_item_id, presence: true
  validates :invoice_item_id, numericality: true
  validates :product_id, presence: true
  validates :product_id, numericality: true
  validates :unit_price_et, presence: true
  validates :vat, presence: true
  validates :unit_price_et, numericality: true
  validates :vat, numericality: true
end
