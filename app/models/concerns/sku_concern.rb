module SkuConcern
  extend ActiveSupport::Concern

  included do
    # validates :sku, presence: true
    validates :sku, uniqueness: true, allow_blank: true

    after_initialize :set_sku
    before_validation :generate_sku, on: :create

  end
    def set_sku
      unless self.sku.present?
        self.sku = Time.now.to_i.to_s
      end
    end

    def generate_sku
      self.sku = self.class.name[0]+Date.today.year.to_s + Date.today.month.to_s.rjust(2,'0') + (self.class.count+1).to_s
      true
    end
end
