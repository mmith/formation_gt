class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true

  validates :street, presence: true
  validates :postal_code, presence: true
  validates :city, presence: true
end
