class Invoice < ApplicationRecord
  include SkuConcern

  belongs_to :order
  belongs_to :customer

  has_many :invoice_items, dependent: :destroy

  validates :customer_id, presence: true
  validates :order_id, presence: true
  validates :customer_id, numericality: true, allow_blank: true
  validates :order_id, numericality: true, allow_blank: true
end
