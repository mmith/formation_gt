# frozen_string_literal: true

module Mutations
  class ProductUpdate < BaseMutation
    description "Updates a product by id"

    field :product, Types::ProductType, null: false

    argument :id, ID, required: true
    argument :label, String, required: true
    argument :unit_price_et, Float, required: true
    argument :vat, Float, required: true

    def resolve(id:, label:, unit_price_et:, vat:)
      product = ::Product.find(id)
      raise GraphQL::ExecutionError.new "Error updating product", extensions: product.errors.to_hash unless product.update(label: label, unit_price_et: unit_price_et, vat: vat)

      { product: product }
    end
  end
end
