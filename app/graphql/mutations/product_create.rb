# frozen_string_literal: true

module Mutations
  class ProductCreate < BaseMutation
    description "Creates a new product"

    field :product, Types::ProductType, null: false

    argument :label, String, required: true
    argument :unit_price_et, Float, required: true
    argument :vat, Float, required: true

    def resolve(label: nil, unit_price_et: nil, vat: nil)
      product = ::Product.new(label: label, unit_price_et: unit_price_et, vat: vat)
      raise GraphQL::ExecutionError.new "Error creating product", extensions: product.errors.to_hash unless product.save

      { product: product }
    end
  end
end
