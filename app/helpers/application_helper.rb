module ApplicationHelper
  def flash_class(level)
    {
      'notice' => 'alert alert-info',
      'success' => 'alert alert-success',
      'error' => 'alert alert-danger',
      'alert' => 'alert alert-warning'
    }[level] || 'alert alert-info'
  end
end
