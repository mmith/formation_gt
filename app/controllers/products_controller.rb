class ProductsController < ApplicationController
  before_action :find_model, except: %w(index new create enableds disableds)

  def index
    @products = Product.all.paginate page: params[:page]

    respond_to do |format|
      format.html{ }

      format.json do
        render json: { products: @products, total_entries: Product.count }
      end
    end
  end

  def enableds
    @products = Product.enableds.paginate page: params[:page]

    respond_to do |format|
      format.html{ }

      format.json do
        render json: { products: @products, total_entries: Product.enableds.count }
      end
    end
  end

  def disableds
    @products = Product.disableds.paginate page: params[:page]

    respond_to do |format|
      format.html{ }

      format.json do
        render json: { products: @products, total_entries: Product.disableds.count }
      end
    end
  end

  def new
    @product = Product.new
    respond_to do |format|
      format.html{ }
      format.json do
        render json: @product
      end
    end
  end

  def create
    @product = Product.new permit_params
    if @product.save
      ActionCable.server.broadcast "product_channel", { message: "Un nouveau produit a été ajouté" }
      respond_to do |format|
        format.html do
          flash[:success] = "Votre enregistrement a été sauvegardé"
          redirect_to products_path
        end
        format.json do
          render json: @product
        end
      end
    else
      respond_to do |format|
        format.html do
          render :new
        end
        format.json do
          render json: @product.errors, status: 406
        end
      end
    end
  end

  def edit
    @product = Product.find params[:id]
    respond_to do |format|
      format.html{ }
      format.json do
        render json: @product
      end
    end
  end

  def update
    if @product.update permit_params
      respond_to do |format|
        format.html do
          flash[:success] = "Votre enregistrement a été sauvegardé"
          redirect_to products_path
        end
        format.json do
          render json: @product
        end
      end
    else
      respond_to do |format|
        format.html do
          render :edit
        end
        format.json do
          render json: @product.errors, status: 406
        end
      end
    end
  end

  def show
    respond_to do |format|
      format.html{ }
      format.json do
        render json: @product
      end
    end
  end

  def disable
    respond_to do |format|
      format.html{ }
      format.json do
        if @product.disable
          render json: @product
        else
          render json: @product.errors, status: 406
        end
      end
    end
  end

  def enable
    respond_to do |format|
      format.html{ }
      format.json do
        if @product.enable
          render json: @product
        else
          render json: @product.errors, status: 406
        end
      end
    end
  end

  def destroy
    respond_to do |format|
      if @product.destroy
        format.html do
          flash[:success] = "Elément supprimé"
          redirect_to products_path
        end
        format.json do
            head :ok
        end
      else
        format.html do
          flash[:error] = "Elément non supprimé: #{@product.errors.full_messages.join(', ')}"
          redirect_to products_path
        end
        format.json do
          render json: @product.errors, status: 406
        end
      end
    end
  end

  private
    def find_model
      @product = Product.find params[:id]
    end

    def permit_params
      params.require(:product).permit(:label, :unit_price_et, :vat)
    end
end
