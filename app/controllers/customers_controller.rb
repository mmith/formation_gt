class CustomersController < ApplicationController
  before_action :find_model, except: %w(index new create)

  def index
    @customers = Customer.all.paginate page: params[:page]

    respond_to do |format|
      format.html{ }

      format.json do
        render json: { customers: @customers, total_entries: Customer.count }
      end
    end
  end

  def new
    @customer = Customer.new
    respond_to do |format|
      format.html{ }
      format.json do
        render json: @customer
      end
    end
  end

  def create
    @customer = Customer.new permit_params
    if @customer.save
      respond_to do |format|
        format.html do
          flash[:success] = "Votre enregistrement a été sauvegardé"
          redirect_to customers_path
        end
        format.json do
          render json: @customer
        end
      end
    else
      respond_to do |format|
        format.html do
          render :new
        end
        format.json do
          render json: @customer.errors, status: 406
        end
      end
    end
  end

  def edit
    @customer = Customer.find params[:id]
    respond_to do |format|
      format.html{ }
      format.json do
        render json: @customer
      end
    end
  end

  def update
    if @customer.update permit_params
      respond_to do |format|
        format.html do
          flash[:success] = "Votre enregistrement a été sauvegardé"
          redirect_to customers_path
        end
        format.json do
          render json: @customer
        end
      end
    else
      respond_to do |format|
        format.html do
          render :edit
        end
        format.json do
          render json: @customer.errors, status: 406
        end
      end
    end
  end

  def show
    respond_to do |format|
      format.html{ }
      format.json do
        render json: @customer
      end
    end
  end


  def destroy
    respond_to do |format|
      if @customer.destroy
        format.html do
          flash[:success] = "Elément supprimé"
          redirect_to customers_path
        end
        format.json do
            head :ok
        end
      else
        format.html do
          flash[:error] = "Elément non supprimé: #{@customer.errors.full_messages.join(', ')}"
          redirect_to customers_path
        end
        format.json do
          render json: @customer.errors, status: 406
        end
      end
    end
  end

  private
    def find_model
      @customer = Customer.find params[:id]
    end

    def permit_params
      params.require(:customer).permit(:first_name, :last_name, :email)
    end
end
