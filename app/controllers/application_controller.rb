class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token, if: :json_request?

  before_action :authenticate_user!, unless: :json_request?

  layout :layout_by_resource

  def json_request?
    request.format.json?
  end

  protected
    def layout_by_resource
      if devise_controller?
        'login'
      else
        'application'
      end
    end


end
