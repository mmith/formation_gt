class AddressesController < ApplicationController
  before_action :find_customer
  before_action :find_model, except: %w(index new create)

  def index
    @addresses = @customer.addresses.all.paginate page: params[:page]

    respond_to do |format|
      format.html{ }

      format.json do
        render json: { addresses: @addresses, total_entries: Address.count }
      end
    end
  end

  def new
    @address = @customer.addresses.new
    respond_to do |format|
      format.html{ }
      format.json do
        render json: @address
      end
    end
  end

  def create
    @address = @customer.addresses.new permit_params
    if @address.save
      respond_to do |format|
        format.html do
          flash[:success] = "Votre enregistrement a été sauvegardé"
          redirect_to customer_addresses_path(@customer)
        end
        format.json do
          render json: @address
        end
      end
    else
      respond_to do |format|
        format.html do
          render :new
        end
        format.json do
          render json: @address.errors, status: 406
        end
      end
    end
  end

  def edit
    respond_to do |format|
      format.html{ }
      format.json do
        render json: @address
      end
    end
  end

  def update
    if @address.update permit_params
      respond_to do |format|
        format.html do
          flash[:success] = "Votre enregistrement a été sauvegardé"
          redirect_to customer_addresses_path(@customer)
        end
        format.json do
          render json: @address
        end
      end
    else
      respond_to do |format|
        format.html do
          render :edit
        end
        format.json do
          render json: @address.errors, status: 406
        end
      end
    end
  end

  def show
    respond_to do |format|
      format.html{ }
      format.json do
        render json: @address
      end
    end
  end


  def destroy
    respond_to do |format|
      if @address.destroy
        format.html do
          flash[:success] = "Elément supprimé"
          redirect_to customer_addresses_path(@customer)
        end
        format.json do
            head :ok
        end
      else
        format.html do
          flash[:error] = "Elément non supprimé: #{@address.errors.full_messages.join(', ')}"
          redirect_to customer_addresses_path(@customer)
        end
        format.json do
          render json: @address.errors, status: 406
        end
      end
    end
  end

  private
    def find_customer
      @customer = Customer.find params[:customer_id]
    end

    def find_model
      @address = @customer.addresses.find params[:id]
    end

    def permit_params
      params.require(:address).permit(:street, :postal_code, :city)
    end
end
